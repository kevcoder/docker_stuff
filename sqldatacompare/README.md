## Create a docker image that uses tlsv1
### This is only required if you receive an error about tls
1. docker build . -t kevin/sqldatacompare-tlsv1


### [SQL Data Compare Commannd Line Documentation](https://documentation.red-gate.com/sdc/using-the-command-line)
```
docker run --rm --interactive --tty --mount type=bind,src=/home/kevin/repos/bitbucket/docker_stuff/sqldatacompare/,dst=/scripts kevin/sqldatacompare-tlsv1 /Argfile:/scripts/sqldatacompare_params.xml
```
