# Create a Sql Server Instance on Azure Container Instances: i.e. a serverless docker container

## [Documentation for ACI YAML](https://docs.microsoft.com/en-us/azure/container-instances/container-instances-reference-yaml)

    ```
    az container create -g rg-kevcoder
    ```