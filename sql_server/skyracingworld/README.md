# Sql Server 217 Developer Edition w/ Sql Agent

## Creating a container from the image

     docker run \
     -e 'ACCEPT_EULA=Y' \
     -e 'MSSQL_SA_PASSWORD=Kevin6669' \
     -p 1433:1433 \
     -v sqlvolume:/var/opt/mssql \
     -d \
     --name mssql_vol_attached  \
     mcr.microsoft.com/mssql/server:2017-latest 

## Copy the scripts into the volume
     docker volumne inspect sqlvolume

     $ cp sudo cp -r /repos/scripts /var/lib/docker/volumes/sqlvolume/_data/scripts

## Creating the database

    docker exec -it sql_server_developer /bin/bash

    root@6f031c7c964f:/# /scripts/build_skyracing_world.sh 

    
