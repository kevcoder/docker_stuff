## Create a docker image that uses tlsv1
1. docker build . -t kevin/sqlcompare-tlsv1

## Create the database
```
docker run --rm --interactive --tty \
--mount type=bind,src=/home/kevin/scripts/sql/srw/host,dst=/scripts \
kevin/sqlcompare-tlsv1 \
/IAgreeToTheEULA  \
/s1:174.143.167.93\\SRWDEVDATA \
/db1:SkyRacingWorld \
/u1:SRWDev \
/p1:my_password \
/empty2 \
/scriptfile:"/scripts/srw_host.sql" 
/Exclude:User /Exclude:Role /Exclude:Schema
```

## Create Migration Script Comparing 2 Databases

### [SQL Compare Commannd Line Documentation](https://documentation.red-gate.com/sc/using-the-command-line)

1. copy the arguments file into the bind mount folder 
    ```
    cp ./sqlcompare_srw_to_azure.xml /home/kevin/scripts/sql/srw/host/
    ```
1. run the image
    ```
    docker run --rm --interactive --tty --mount type=bind,src=/home/kevin/repos/bitbucket/docker_stuff/sqlcompare/,dst=/scripts kevin/sqlcompare-tlsv1 /Argfile:/scripts/sqlcompare_params.xml
    ```
